//Khai báo đối tượng Todo item để lưu trữ thông tin
function TODO_ITEM () {
	this.ID='';//khai báo ID của item
	this.NAME='';//Khai báo nội dung của item
	this.ORDER=0;//khai báo chỉ số sắp xếp
	this.CHECKED=0;//khai báo giá trị check cho item

	this.setID=function setID(id){
		this.ID=id;
	}
	this.setNAME=function setNAME(name){
		this.NAME=name;
	}
	this.setORDER=function setORDER(order){
		this.ORDER=order;
	}
	this.setCHECKED=function setCHECKED(checked){
		this.CHECKED=checked;
	}

	this.getID=function getID(){
		return this.ID;
	}
	this.getNAME=function getNAME(){
		return this.NAME;
	}
	this.getORDER=function getORDER(){
		return this.ORDER;
	}
	this.getCHECKED=function getCHECKED(){
		return this.CHECKED;
	}

	this.createNEW=function createNEW(id, name, order, checked){
		this.ID=id;
		this.NAME=name;
		this.ORDER=order;
		this.CHECKED=checked;
	}

}

var ITEM_LIST= new TODO_ITEM();//tạo mới một đối tượng
ITEM_LIST=[];//khai báo mảng đối tượng
var countOrder=0;//khai báo biến đếm
var lastnode=null;//được sử dụng để lưu trữ node element


//Hàm thêm mới một đối tượng vào mảng
//giá trị được lấy từ input
function addITEM(){
	var item=new TODO_ITEM();
	item.setID(Date().toLocaleString());
	item.setNAME(document.getElementById("inputNewTask").value);
	item.setORDER(countOrder++);
	item.setCHECKED(0);
	ITEM_LIST.push(item);
	showALL();//hiển thị danh sách các item
	document.getElementById("inputNewTask").value="";
	submitData(item.getNAME());//đưa dữ liệu lên Parse
}


//hàm xóa một Item trong mảng dựa vào chỉ số
function deleteITEM(id){
	ITEM_LIST.splice(id,1);
	showALL();
}

//hàm thực hiện việc edit item khi người dùng click chuột vào item
function editITEM(id){
	var value=prompt("Input to Edit", ITEM_LIST[id].getNAME());//hiển thị hộp thoại thông báo cho người dùng nhập giá trị cần edit
	if (value!=null){
		ITEM_LIST[id].setNAME(value);
		showALL();
	}
}



function setCheck(id){
	if (document.getElementById("checkbox"+id).checked)
	{
		ITEM_LIST[id].setCHECKED(1);
		console.log(ITEM_LIST[id].getCHECKED());
	}
	else
	{
		ITEM_LIST[id].setCHECKED(0);
		console.log(ITEM_LIST[id].getCHECKED());
	}
}

function showALL(){
	var showList = document.getElementById("showListItem");
	showList.innerHTML="";
	for (var i=0; i<ITEM_LIST.length; i++){
		var html='<div class="item"  draggable="true" onmousedown="mouseDown(event)" onmouseup="mouseUp(event)" ondragover="mouseUp(event)">';
			html+='<input type="checkbox" id="checkbox'+i+'" onclick="setCheck('+i+')"/>';
			html+='<label onclick="editITEM('+i+')" >Content_label</label>';
			html+='<button  onclick="deleteITEM('+ i +')"></button>';
			html+='</div>';
		html=html.replace("Content_label",ITEM_LIST[i].getNAME());
		showList.innerHTML+=html;
		console.log(ITEM_LIST[i].getNAME()+ITEM_LIST[i].getCHECKED());
		if (ITEM_LIST[i].getCHECKED()==1) {document.getElementById("checkbox"+i).checked=true;}
		console.log(document.getElementById("checkbox"+i).checked);
	}
	
}

function deleteALL(){
	for (var i=0; i<ITEM_LIST.length;){
		ITEM_LIST.splice(i,1);
	}
	showALL();
}

function mouseDown(event){
	lastnode = event.target;
}

function mouseUp(event){
	lastnode.parentNode.insertBefore(lastnode,event.target);
}


function submitData(name, i)
{
	Parse.initialize("viBknTboYGoQRZii7nHKfmGctcDBcYsQf1gCAq84", 
	"weivFDNQcILNrhcPCa1bZMf0aut0k1WWUXEqfKiQ");
	var user = new Parse.User();
	user.set("username", "minhtu");
	user.set("password", "minhtu");
	var currentUser = Parse.User.current();
	Parse.User.logIn("minhtu", "minhtu", {
	success: function(user) {
	// Do stuff after successful login.
	},
	error: function(user, error) {
	// The login failed. Check error to see why.    
	alert("fail");
	}
	});
	var todo_parse = Parse.Object.extend("Todo");
	// Create a new instance of that class.
	var todop = new todo_parse();
	// Alternatively, you can use the typical Backbone syntax.
	todop.set({"Order": i});
	todop.set({"Task": name});
	todop.set({"User": currentUser});
	todop.save(null, {
	success: function(user) {
	// Execute any logic that should take place after the object is saved.
	},
	error: function(user, error) {
	// Execute any logic that should take place if the save fails.
	// error is a Parse.Error with an error code and message.
	alert('Failed to create new object, with error code: ');
	}
	});
}

